1. display xx
2. sub <-- this is the one that updates individual row amount!!!
3. calculate xx because its actually updating total amount

on changing book
--> display executes
  -- update price textbox
  -- enable/ disable quantity drop down
  --> sub executes
    -- what does sub do?
    -- it updates the amount
    -- it updates the total amount too (because it calls calculate inside)

calculate only updates total amount

====================================

alert("hello this is an alert")
// no alert

var x = confirm("are you sure about this?")
// x stored the user feedback 
// if chosen yes, x = true
// if chosen no, x = false

var x = prompt("what's you name?")
// x stores the feedback from user (string), if user types in and choose yes
// x stores null if user choose no (regardless if typed in or not)












